import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ContactsComponent } from './contacts/contacts.component';
import { UsersComponent } from './users/users.component';
import { LogoutComponent } from './logout/logout.component';

const routes: Routes = [
  {path:"Home",component:HomeComponent},
  {path:"Contacts" ,component:ContactsComponent},
  {path:"Users", component:UsersComponent},
  {path:"Logout", component:LogoutComponent},
  {path:"",redirectTo:"Home",pathMatch:"full"}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
